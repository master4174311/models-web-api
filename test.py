import json
import pandas as pd
import joblib
from flask import jsonify


def decision(new_data_path, decision_model_file_path, data_path):
    # Load the data for prediction
    new_data_predict = pd.read_csv(new_data_path)

    # Load the trained Decision Tree model
    loaded_model = joblib.load('/home/huynt/bin/src/git/models-web-api/decision_tree_model.pkl')

    # Drop unnecessary columns
    columns_to_drop = ['Source IP', 'Destination IP', 'Source Port', 'Protocol', 'TimeSpan']
    new_data_predict = new_data_predict.drop(columns=columns_to_drop)

    # Add a new column named 'Label' with null values
    new_data_predict['Label'] = None

    # Predict using the trained model
    predicted_labels = loaded_model.predict(new_data_predict.drop(columns=['Label']))

    # Assign the predicted labels to the 'Label' column
    new_data_predict['Label'] = predicted_labels

    # Reverse label encoding if needed
    reverse_label_map = {1: 'Anomaly', 0: 'Normal'}
    new_data_predict['Label'] = new_data_predict['Label'].map(reverse_label_map)

    # Display the predicted data
    # print(new_data_predict['Destination Port'])

    # Load the original data
    data = pd.read_csv(data_path)

    # Filter rows in the original data with the same 'Destination Port' as new_data_predict and label them as 'Anomaly'
    anomaly_data = data[data['Destination Port'] == new_data_predict['Destination Port']]
    anomaly_data['Label'] = 'Anomaly'

    anomaly_data.head(1)
    # Print the filtered data
    # print(anomaly_data[['Source IP', 'Destination IP', 'Destination Port', 'Source Port', 'Protocol', 'TimeSpan', 'Label']].head(3))
    out = anomaly_data.head(1).to_json(orient='records')[1:-1].replace('},{', '} {')

    # Return the anomaly data as JSON
    return out



print('hello')
out = decision('datatest.csv', 'decision_tree_model.pkl', 'datatest.csv')
print(out)
