from flask_restx import Namespace, Resource, reqparse, fields
from flask import jsonify, request
import pandas as pd
import os
import joblib
from sklearn.neighbors import NearestNeighbors
from sklearn.preprocessing import StandardScaler
import pymysql
from services.knn_service import *


knn_namespace = Namespace('api/knn', description='KNN operations')

query_similar = knn_namespace.model('SimilarModel', {
    'productId': fields.List(fields.String,  description='List of input queries')

})


@knn_namespace.route('/train/<string:kCluster>')
class TrainKnn(Resource):
    def get(self, kCluster):
        # parser = reqparse.RequestParser()
        # parser.add_argument('kCluster', type=str)
        return train_knn_model(kCluster)


@knn_namespace.route('/train/by_cpu/<string:kCluster>')
class TrainKnnByCPU(Resource):
    def get(self, kCluster):
        parser = reqparse.RequestParser()
        parser.add_argument('kCluster', type=str)
        return retrain_knn_model_by_params_cpu(kCluster)


@knn_namespace.route('/train/by_price/<string:kCluster>')
class TrainKnnByPrice(Resource):
    def get(self, kCluster):
        parser = reqparse.RequestParser()
        parser.add_argument('kCluster', type=str)
        return retrain_knn_model_by_params_price(kCluster)


@knn_namespace.route('/train/by_weight/<string:kCluster>')
class TrainKnnByWeight(Resource):
    def get(self, kCluster):
        parser = reqparse.RequestParser()
        parser.add_argument('kCluster', type=str)
        return retrain_knn_model_by_params_weight(kCluster)


@knn_namespace.route('/train/by_screen/<string:kCluster>')
class TrainKnnByScreen(Resource):
    def get(self, kCluster):
        parser = reqparse.RequestParser()
        parser.add_argument('kCluster', type=str)
        return retrain_knn_model_by_params_screen(kCluster)


@knn_namespace.route('/train/by_ram/<string:kCluster>')
class TrainKnnByRam(Resource):
    def get(self, kCluster):
        parser = reqparse.RequestParser()
        parser.add_argument('kCluster', type=str)
        return retrain_knn_model_by_params_ram(kCluster)


@knn_namespace.route('/train/by_storage/<string:kCluster>')
class TrainKnnByStorage(Resource):
    def get(self, kCluster):
        parser = reqparse.RequestParser()
        parser.add_argument('kCluster', type=str)
        return retrain_knn_model_by_params_storage(kCluster)


@knn_namespace.route('/train/by_graphics/<string:kCluster>')
class TrainKnnByGraphics(Resource):
    def get(self, kCluster):
        parser = reqparse.RequestParser()
        parser.add_argument('kCluster', type=str)
        return retrain_knn_model_by_params_graphics(kCluster)


@knn_namespace.route('/retrain/<string:kCluster>')
class ReTrainKnn(Resource):
    def get(self, kCluster):
        parser = reqparse.RequestParser()
        parser.add_argument('kCluster', type=str)
        return retrain_knn_model(kCluster)


@knn_namespace.route('/similar/<string:productId>')
class ProductsSimilar(Resource):
    def get(self, productId):
        return similar(productId)


@knn_namespace.route('/similar/cpu')
class ProductsSimilarCPU(Resource):
    @knn_namespace.expect(query_similar, validate=False)
    def post(self):
        productId = request.json.get('productId', '')

        return similar_cpu(productId)


@knn_namespace.route('/similar/weight')
class ProductsSimilarWeight(Resource):
    @knn_namespace.expect(query_similar, validate=False)
    def post(self):
        productId = request.json.get('productId', '')
        return similar_weight(productId)


@knn_namespace.route('/similar/ram')
class ProductsSimilarRam(Resource):
    @knn_namespace.expect(query_similar, validate=False)
    def post(self):
        productId = request.json.get('productId', '')
        return similar_ram(productId)


@knn_namespace.route('/similar/screen')
class ProductsSimilarScreen(Resource):
    @knn_namespace.expect(query_similar, validate=False)
    def post(self):
        productId = request.json.get('productId', '')
        return similar_screen(productId)


@knn_namespace.route('/similar/storage')
class ProductsSimilarStorage(Resource):
    @knn_namespace.expect(query_similar, validate=False)
    def post(self):
        productId = request.json.get('productId', '')
        return similar_storage(productId)


@knn_namespace.route('/similar/graphics')
class ProductsSimilarGraphics(Resource):
    @knn_namespace.expect(query_similar, validate=False)
    def post(self):
        productId = request.json.get('productId', '')
        return similar_graphics(productId)


@knn_namespace.route('/similar/price/<string:productId>')
class ProductsSimilarPrice(Resource):
    def get(self, productId):
        return similar_price(productId)
