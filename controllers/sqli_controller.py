from flask_restx import Namespace, Resource,reqparse,fields
from flask import jsonify, request
import pandas as pd
import os
import joblib
from sklearn.neighbors import NearestNeighbors
from sklearn.preprocessing import StandardScaler
import pymysql
from services.sqli_service import *

sqli_namespace = Namespace('api/sqli', description='SQLI operations')
# Định nghĩa mô hình dữ liệu cho thông số của mô hình
query_model = sqli_namespace.model('QueryTrainModel', {
    'max_words': fields.Integer(required=True, description='Maximum number of words', default=5000),
    'max_len': fields.Integer(required=True, description='Maximum length', default=100),
    'test_size': fields.Float(required=True, description='Test size', default=0.2),
    'random_state': fields.Integer(required=True, description='Random state', default=42),
    'epochs': fields.Integer(required=False, description='Number of epochs', default=5),
    'batch_size': fields.Integer(required=False, description='Batch size', default=32),
    'validation_split': fields.Float(required=False, description='Validation split', default=0.2),
    'output_dim': fields.Integer(required=False, description='Output dimension', default=128),
    'unitRNN': fields.Integer(required=False, description='Number of units in RNN layer', default=64),
    'unitDense': fields.Integer(required=False, description='Number of units in Dense layer', default=1)
})


query_check_sqli = sqli_namespace.model('QueryModel', {
    'queries': fields.List(fields.String, required=True, description='List of input queries'),
    'max_len': fields.Integer(required=True, description='Maximum length'),
    'max_words': fields.Integer(required=True, description='Maximum number of words')

})


@sqli_namespace.route('/train')
class TrainRnn(Resource):
    @sqli_namespace.expect(query_model)  # Chỉ định mô hình dữ liệu cho phương thức POST
    def post(self):
        # Lấy dữ liệu từ body JSON của yêu cầu
        request_data = request.get_json()

        # Kiểm tra xem các thông số cần thiết đã được gửi trong yêu cầu hay không
        if 'max_words' not in request_data or 'max_len' not in request_data or 'test_size' not in request_data or 'random_state' not in request_data:
            return {'error': 'Missing required parameters'}, 400

        # Lấy các thông số từ dữ liệu yêu cầu
        max_words = request_data['max_words']
        max_len = request_data['max_len']
        test_size = request_data['test_size']
        random_state = request_data['random_state']

        # Gọi hàm train_sqli_model() với các thông số đã lấy từ yêu cầu
        return train_sqli_model(max_words, max_len, test_size, random_state)

@sqli_namespace.route('/retrain')
class ReTrainRnn(Resource):
    @sqli_namespace.expect(query_model)  
    def post(self):
        # Lấy dữ liệu từ body JSON của yêu cầu
        request_data = request.get_json()

        # # Kiểm tra xem các thông số cần thiết đã được gửi trong yêu cầu hay không
        # if 'max_words' not in request_data or 'max_len' not in request_data or 'test_size' not in request_data or 'random_state' not in request_data:
        #     return {'error': 'Missing required parameters'}, 400

        max_words = int(request_data['max_words'])
        max_len = int(request_data['max_len'])
        test_size = float(request_data['test_size'])  # Parse to float for test_size
        random_state = int(request_data['random_state'])

        # Gọi hàm retrain_sqli_model() với các thông số đã lấy từ yêu cầu
        epochs = int(request_data.get('epochs', 5))
        batch_size = int(request_data.get('batch_size', 32))
        validation_split = float(request_data.get('validation_split', 0.2))  # Parse to float for validation_split
        output_dim = int(request_data.get('output_dim', 128))
        unitRNN = int(request_data.get('unitRNN', 64))
        unitDense = int(request_data.get('unitDense', 1))


        return retrain_sqli_model(max_words=max_words, max_len=max_len, test_size=test_size, random_state=random_state,
                                  epochs=epochs, batch_size=batch_size, validation_split=validation_split,
                                  output_dim=output_dim, unitRNN=unitRNN, unitDense=unitDense)
@sqli_namespace.route('/check')
class SQLICheck(Resource):
    @sqli_namespace.expect(query_check_sqli, validate=True)
    def post(self):
        try:
            # Get input data from the request
            max_words = request.json.get('max_words', 5000)
            max_len = request.json.get('max_len', 100)
            test_queries = request.json.get('queries', [])

            response = check_sqli(test_queries,max_len,max_words)
            return {'results': response}, 200

        except Exception as e:
            return {'error': str(e)}, 500
        
@sqli_namespace.route('/info')
class SQLiModelInfo(Resource):
    def get(self):
        try:
            if os.path.exists(sqli_model_file_path):
                # Load the trained model
                loaded_model = load_model(sqli_model_file_path)

                # Get model architecture summary
                model_summary = []
                loaded_model.summary(print_fn=lambda x: model_summary.append(x))

                # Get training configuration
                optimizer = loaded_model.optimizer.__class__.__name__
                loss = loaded_model.loss
                metrics = loaded_model.metrics_names
                batch_size = loaded_model._feed_input_names[0] if loaded_model._feed_input_names else None
                # epochs = loaded_model.history.params['epochs']

                # Get evaluation metrics
                # evaluation_metrics = loaded_model.evaluate(X_test_padded, y_test)

                # Construct model info dictionary
                model_info = {
                    'architecture_summary': model_summary,
                    'training_configuration': {
                        'optimizer': optimizer,
                        'loss': loss,
                        'metrics': metrics,
                        'batch_size': batch_size,
                        # 'epochs': epochs
                    }
                    # 'evaluation_metrics': {
                    #     'accuracy': evaluation_metrics[1],
                    #     # 'loss': evaluation_metrics[0]
                    # }
                }

                return {'model_info': model_info}, 200
            else:
                return {'error': 'Model not found. Please train the model first.'}, 404
        except Exception as e:
            return {'error': str(e)}, 500
        
