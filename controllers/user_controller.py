from flask_restx import Namespace,Resource

user_namespace = Namespace('user', description='User operations')

@user_namespace.route('/api/users')
class UserList(Resource):
    def get(self):
        # Logic to fetch users from the database
        users = [{'id': 1, 'name': 'John'}, {'id': 2, 'name': 'Alice'}]
        return {'users': users}

# Other user-related routes can be defined here
