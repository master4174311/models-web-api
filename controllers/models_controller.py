from flask_restx import Namespace, Resource
from flask import jsonify
import os

model_directory = "common/models/"
models_info = [
    {'name': 'KNN Model', 'type': 'knn', 'file': 'knn_model.pkl'},
    {'name': 'SQLi RNN Model', 'type': 'sqli_rnn', 'file': 'sqli_rnn_model.h5'},
    {'name': 'XSS RNN Model', 'type': 'xss_rnn', 'file': 'xss_rnn_model.h5'}
]

models_namespace = Namespace('api/models', description='Models operations')

@models_namespace.route('/info')
class GetInfoModels(Resource):
    def post(self):
        models = []

        for model_info in models_info:
            model_path = os.path.join(model_directory, model_info['file'])
            is_exist = os.path.exists(model_path)
            model = {
                'name': model_info['name'],
                'type': model_info['type'],
                'isExist': 1 if is_exist else 0
            }
            models.append(model)

        return jsonify(models)
