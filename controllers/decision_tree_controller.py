from flask_restx import Namespace, Resource,reqparse
from flask import jsonify, request
import pandas as pd
import os
import joblib
from sklearn.neighbors import NearestNeighbors
from sklearn.preprocessing import StandardScaler
import pymysql
from services.decision_tree_service import *


decision_tree_namespace = Namespace('api/decision-tree', description='Decision tree operations')
# @decision_tree_namespace.route('/train/<string:kCluster>')
# class TrainKnn(Resource):
#     def post(self, kCluster):
#         parser = reqparse.RequestParser()
#         parser.add_argument('kCluster', type=str)
#         return train_knn_model(kCluster)

# @decision_tree_namespace.route('/retrain/<string:kCluster>')
# class ReTrainKnn(Resource):
#     def post(self,kCluster):
#         parser = reqparse.RequestParser()
#         parser.add_argument('kCluster', type=str)
#         return retrain_knn_model(kCluster)
        

# @decision_tree_namespace.route('/similar/<string:productId>')
# class ProductsSimilar(Resource):
#     def get(self, productId):
#         return similar(productId)

    
        
@decision_tree_namespace.route('/detection/<string:productId>')
class DDosDetection(Resource):
    def get(self, productId):
        return detection()
