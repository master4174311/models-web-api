from flask_restx import Namespace, Resource,reqparse,fields
from flask import jsonify, request
import pandas as pd
import os
import joblib
from sklearn.neighbors import NearestNeighbors
from sklearn.preprocessing import StandardScaler
import pymysql
from services.xss_service import *

xss_namespace = Namespace('api/xss', description='XSS operations')
query_model = xss_namespace.model('QueryTrainModel', {
    'max_words': fields.Integer(required=True, description='Maximum number of words', default=5000),
    'max_len': fields.Integer(required=True, description='Maximum length', default=100),
    'test_size': fields.Float(required=True, description='Test size', default=0.2),
    'random_state': fields.Integer(required=True, description='Random state', default=42),
    'epochs': fields.Integer(required=False, description='Number of epochs', default=5),
    'batch_size': fields.Integer(required=False, description='Batch size', default=32),
    'validation_split': fields.Float(required=False, description='Validation split', default=0.2),
    'output_dim': fields.Integer(required=False, description='Output dimension', default=128),
    'unitRNN': fields.Integer(required=False, description='Number of units in RNN layer', default=64),
    'unitDense': fields.Integer(required=False, description='Number of units in Dense layer', default=1)
})


query_check_xss = xss_namespace.model('QueryModel', {
    'queries': fields.List(fields.String, required=True, description='List of input queries'),
    'max_len': fields.Integer(required=True, description='Maximum length'),
    'max_words': fields.Integer(required=True, description='Maximum number of words')


})

@xss_namespace.route('/train')
class TrainRnn(Resource):
    @xss_namespace.expect(query_model)  # Chỉ định mô hình dữ liệu cho phương thức POST
    
    def post(self):
        data = request.json  # Lấy dữ liệu từ phần thân JSON của yêu cầu
        max_words = data.get('max_words')  # Lấy giá trị của max_words từ dữ liệu JSON
        max_len = data.get('max_len')  # Lấy giá trị của max_len từ dữ liệu JSON
        test_size = data.get('test_size')  # Lấy giá trị của test_size từ dữ liệu JSON
        random_state = data.get('random_state')  # Lấy giá trị của random_state từ dữ liệu JSON

        # Gọi hàm train_xss_model với các tham số đã lấy từ phần thân JSON
        return train_xss_model(max_words, max_len, test_size, random_state)

@xss_namespace.route('/retrain')
class ReTrainRnn(Resource):
    @xss_namespace.expect(query_model)  # Chỉ định mô hình dữ liệu cho phương thức POST

    def post(self):
        # Lấy dữ liệu từ body JSON của yêu cầu
        request_data = request.get_json()

        max_words = int(request_data['max_words'])
        max_len = int(request_data['max_len'])
        test_size = float(request_data['test_size'])  # Parse to float for test_size
        random_state = int(request_data['random_state'])

        # Gọi hàm retrain_sqli_model() với các thông số đã lấy từ yêu cầu
        epochs = int(request_data.get('epochs', 5))
        batch_size = int(request_data.get('batch_size', 32))
        validation_split = float(request_data.get('validation_split', 0.2))  # Parse to float for validation_split
        output_dim = int(request_data.get('output_dim', 128))
        unitRNN = int(request_data.get('unitRNN', 64))
        unitDense = int(request_data.get('unitDense', 1))

        # Gọi hàm retrain_xss_model() với các thông số đã lấy từ yêu cầu
        return retrain_xss_model(max_words=max_words, max_len=max_len, test_size=test_size, random_state=random_state,
                                  epochs=epochs, batch_size=batch_size, validation_split=validation_split,
                                  output_dim=output_dim, unitRNN=unitRNN, unitDense=unitDense)
        
@xss_namespace.route('/check')
class XSSCheck(Resource):
    @xss_namespace.expect(query_check_xss, validate=True)
    def post(self):
        try:
            # Get input data from the request
            max_words = request.json.get('max_words', 5000)
            max_len = request.json.get('max_len', 100)
            test_queries = request.json.get('queries', [])

            response = check_xss(test_queries,max_len,max_words)
            return {'results': response}, 200


        except Exception as e:
            return {'error': str(e)}, 500
        
@xss_namespace.route('/info')
class SQLiModelInfo(Resource):
    def get(self):
        try:
            if os.path.exists(xss_model_file_path):
                # Load the trained model
                loaded_model = load_model(xss_model_file_path)

                # Get model architecture summary
                model_summary = []
                loaded_model.summary(print_fn=lambda x: model_summary.append(x))

                # Get model architecture summary
                model_summary = []
                loaded_model.summary(print_fn=lambda x: model_summary.append(x))

                # Convert model summary to a single string
                model_summary_str = '\n'.join(model_summary)

                # Construct model info dictionary
                model_info = {
                    'architecture_summary': model_summary_str,
                    'training_configuration': sqli_training_params,  # Use stored training parameters
                    # No need to evaluate the model again, as we already have the evaluation metrics from training
                    'evaluation_metrics': {
                        'accuracy': sqli_training_params.get('accuracy', None),
                        'loss': sqli_training_params.get('loss', None)
                    }
                }

                return jsonify({'model_info': model_info}), 200
            else:
                return {'error': 'Model not found. Please train the model first.'}, 404
        except Exception as e:
            return {'error': str(e)}, 500
        
        