import smtplib
from email.mime.text import MIMEText
import json
import pandas as pd
import joblib
from flask import jsonify


def decision(new_data_path, decision_model_file_path, data_path):
    # Load the data for prediction
    new_data_predict = pd.read_csv(new_data_path)

    # Load the trained Decision Tree model
    loaded_model = joblib.load('/home/huynt/bin/src/git/models-web-api/decision_tree_model.pkl')

    # Drop unnecessary columns
    columns_to_drop = ['Source IP', 'Destination IP', 'Source Port', 'Protocol', 'TimeSpan']
    new_data_predict = new_data_predict.drop(columns=columns_to_drop)

    # Add a new column named 'Label' with null values
    new_data_predict['Label'] = None

    # Predict using the trained model
    predicted_labels = loaded_model.predict(new_data_predict.drop(columns=['Label']))

    # Assign the predicted labels to the 'Label' column
    new_data_predict['Label'] = predicted_labels

    # Reverse label encoding if needed
    reverse_label_map = {1: 'Anomaly', 0: 'Normal'}
    new_data_predict['Label'] = new_data_predict['Label'].map(reverse_label_map)

    # Display the predicted data
    # print(new_data_predict['Destination Port'])

    # Load the original data
    data = pd.read_csv(data_path)

    # Filter rows in the original data with the same 'Destination Port' as new_data_predict and label them as 'Anomaly'
    anomaly_data = data[data['Destination Port'] == new_data_predict['Destination Port']]
    anomaly_data['Label'] = 'Anomaly'

    anomaly_data.head(1)
    # Print the filtered data
    # print(anomaly_data[['Source IP', 'Destination IP', 'Destination Port', 'Source Port', 'Protocol', 'TimeSpan', 'Label']].head(3))
    out = anomaly_data.head(2).to_json(orient='records')

    # Return the anomaly data as JSON
    return out



out = decision('datatest.csv', 'decision_tree_model.pkl', 'datatest.csv')

# Email configuration
sender_email = "huynguyen20002023@gmail.com"
sender_password = "lxrs gpvk ohxn zjgv"
recipient_email = "huynguyen20002023@gmail.com"
subject = "Anomaly Detection Alert"

try:
    # Convert the JSON string to a dictionary
    anomaly_data_dict = json.loads(out)

    # HTML template for the email body
    html_body = """
    <html>
      <body>
        <h2>Anomaly Detection Alert</h2>
        <p>Below is the detected anomaly:</p>
        <table border="1">
          <tr>
            <th>Source IP</th>
            <th>Destination IP</th>
            <th>Source Port</th>
            <th>Destination Port</th>
            <th>Protocol</th>
            <th>TimeSpan</th>
            <th>Label</th>
          </tr>
    """
    print(anomaly_data_dict)
    # Add data rows to the table
    for record in anomaly_data_dict:
        html_body += f"""
          <tr>
            <td>{record['Source IP']}</td>
            <td>{record['Destination IP']}</td>
            <td>{record['Source Port']}</td>
            <td>{record['Destination Port']}</td>
            <td>{record['Protocol']}</td>
            <td>{record['TimeSpan']}</td>
            <td>{record['Label']}</td>
          </tr>
    """

    # Close the table and body tags
    html_body += """
        </table>
      </body>
    </html>
    """

    # Create the MIMEText object with HTML content
    html_message = MIMEText(html_body, 'html')
    html_message['Subject'] = subject
    html_message['From'] = sender_email
    html_message['To'] = recipient_email

    # Send the email
    with smtplib.SMTP_SSL('smtp.gmail.com', 465) as server:
        server.login(sender_email, sender_password)
        server.sendmail(sender_email, recipient_email, html_message.as_string())

    print("Email sent successfully!")

except json.JSONDecodeError as e:
    print(f"Error decoding JSON: {e}")
except Exception as e:
    print(f"An error occurred: {e}")