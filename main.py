from flask import Flask, jsonify
import pandas as pd
import pymysql
import numpy as np
import os
import joblib
from sklearn.neighbors import NearestNeighbors
from sklearn.preprocessing import StandardScaler

app = Flask(__name__)

# Define database configuration
DB_HOST = 'localhost'
DB_PORT = 3306
DB_USER = 'root'
DB_PASS = '123456'
DB_NAME = 'ecommerce'

# Load KNN model
knn_model_directory = "/Users/nguyenthanhhuy/PycharmProjects/Kmeans/"
knn_model_file_path = os.path.join(knn_model_directory, "knn_model.pkl")

if os.path.exists(knn_model_file_path):
    knn = joblib.load(knn_model_file_path)


@app.route('/')
def hello():
    return "Hello, World!"


@app.route('/info', methods=['GET'])
def get_model_info():
    if os.path.exists(knn_model_file_path):
        # Retrieve information about the KNN model
        model_info = {
            "algorithm": knn.algorithm,
            "n_neighbors": knn.n_neighbors,
            "metric": knn.metric,
            "effective_metric_params": knn.effective_metric_params_,
            "leaf_size": knn.leaf_size,
            "metric_params": knn.metric_params,
            "effective_metric_": knn.effective_metric_,
            # Add more attributes as needed
        }
        return jsonify(model_info), 200
    else:
        return jsonify({'error': 'Model not found. Please train the model first.'}),

@app.route('/train/<kNeighbors>', methods=['POST'])
def train_model(kNeighbors):
    if os.path.exists(knn_model_file_path):
        return jsonify({'message': 'Model already trained. No need to train again.'}), 200
    else:
        # Load dữ liệu từ tập tin csv
        df = pd.read_csv(os.path.join(knn_model_directory, "dataset.csv"), header=0, na_values="NA", comment='\t', sep=',', skipinitialspace=True, engine='python')

        # Mã hoá dữ liệu sản phẩm
        categorical_cols = ['cpu', 'graphics', 'ram', 'screen', 'storage', 'weight']
        df[categorical_cols] = df[categorical_cols].astype('category')
        df[categorical_cols] = df[categorical_cols].apply(lambda x: x.cat.codes)

        # Chuẩn bị dữ liệu đầu vào cho KNN
        X = df[['rating', 'base_price', 'discount_price', 'promotion_price', 'cpu', 'graphics', 'ram', 'screen', 'storage', 'weight']]
        scaler = StandardScaler()
        X_scaled = scaler.fit_transform(X)
        k = int(kNeighbors)

        # Huấn luyện mô hình KNN
        knn = NearestNeighbors(n_neighbors=k, algorithm='auto')
        knn.fit(X_scaled)

        # Lưu trữ mô hình đã huấn luyện
        joblib.dump(knn, knn_model_file_path)

        return jsonify({'message': f'Model trained successfully with k: {k}'}), 200


@app.route('/retrain/<kNeighbors>', methods=['POST'])
def retrain_model(kNeighbors):
    with pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASS, db=DB_NAME, charset='utf8',
                         cursorclass=pymysql.cursors.DictCursor) as mysql_connection:
        updated_df = "SELECT p.id, p.name, p.rating, pp.base_price, pp.discount_price, pp.promotion_price, " \
                             "MAX(CASE WHEN a.name = 'CPU' THEN pa.value END) AS cpu, " \
                             "MAX(CASE WHEN a.name = 'Graphics' THEN pa.value END) AS graphics, " \
                             "MAX(CASE WHEN a.name = 'RAM' THEN pa.value END) AS ram, " \
                             "MAX(CASE WHEN a.name = 'Screen' THEN pa.value END) AS screen, " \
                             "MAX(CASE WHEN a.name = 'Storage' THEN pa.value END) AS storage, " \
                             "MAX(CASE WHEN a.name = 'Weight' THEN pa.value END) AS weight " \
                             "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                             "INNER JOIN products p ON p.id = pa.product_id " \
                             "INNER JOIN product_price pp ON pp.product_id = p.id " \
                             "GROUP BY p.name, p.rating, pp.base_price, pp.discount_price, " \
                             "pp.promotion_price"
        # all_products_info = fetch_product_data(mysql_connection, query_all_products, productId)

        # Load updated dataset for training
        updated_df = pd.read_csv("/Users/nguyenthanhhuy/PycharmProjects/Kmeans/dataset.csv", header=0, na_values="NA", comment='\t', sep=',', skipinitialspace=True, engine='python')

        # Mã hoá dữ liệu sản phẩm
        categorical_cols = ['cpu', 'graphics', 'ram', 'screen', 'storage', 'weight']
        updated_df[categorical_cols] = updated_df[categorical_cols].astype('category')
        updated_df[categorical_cols] = updated_df[categorical_cols].apply(lambda x: x.cat.codes)

        # Chuẩn bị dữ liệu đầu vào cho KNN
        X_updated = updated_df[['rating', 'base_price', 'discount_price', 'promotion_price', 'cpu', 'graphics', 'ram', 'screen', 'storage', 'weight']]
        scaler_updated = StandardScaler()
        X_scaled_updated = scaler_updated.fit_transform(X_updated)
        k = int(kNeighbors)

        # Huấn luyện lại mô hình KNN
        knn_updated = NearestNeighbors(n_neighbors=k, algorithm='auto')
        knn_updated.fit(X_scaled_updated)

        # Lưu trữ mô hình đã huấn luyện
        joblib.dump(knn_updated, knn_model_file_path)

        return jsonify({'message': f'Model retrained successfully with k: {kNeighbors}'})


@app.route('/similar/<productId>', methods=['GET'])
def get_similar(productId):
    try:
        if os.path.exists(knn_model_file_path):
            knn = joblib.load(knn_model_file_path)
        else:
            return jsonify({'Not find model. Please training model'}), 404

        with pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASS, db=DB_NAME, charset='utf8',
                             cursorclass=pymysql.cursors.DictCursor) as mysql_connection:
            query_single_product = "SELECT p.id, p.name, p.rating, pp.base_price, pp.discount_price, " \
                                   "pp.promotion_price, MAX(CASE WHEN a.name = 'CPU' THEN pa.value END) AS cpu, " \
                                   "MAX(CASE WHEN a.name = 'Graphics' THEN pa.value END) AS graphics, " \
                                   "MAX(CASE WHEN a.name = 'RAM' THEN pa.value END) AS ram, " \
                                   "MAX(CASE WHEN a.name = 'Screen' THEN pa.value END) AS screen, " \
                                   "MAX(CASE WHEN a.name = 'Storage' THEN pa.value END) AS storage, " \
                                   "MAX(CASE WHEN a.name = 'Weight' THEN pa.value END) AS weight " \
                                   "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                                   "INNER JOIN products p ON p.id = pa.product_id " \
                                   "INNER JOIN product_price pp ON pp.product_id = p.id " \
                                   "WHERE p.id  = %s GROUP BY p.id, p.name, p.rating, pp.base_price, " \
                                   "pp.discount_price, pp.promotion_price"

            query_all_products = "SELECT p.id, p.name, p.rating, pp.base_price, pp.discount_price, pp.promotion_price, " \
                                 "MAX(CASE WHEN a.name = 'CPU' THEN pa.value END) AS cpu, " \
                                 "MAX(CASE WHEN a.name = 'Graphics' THEN pa.value END) AS graphics, " \
                                 "MAX(CASE WHEN a.name = 'RAM' THEN pa.value END) AS ram, " \
                                 "MAX(CASE WHEN a.name = 'Screen' THEN pa.value END) AS screen, " \
                                 "MAX(CASE WHEN a.name = 'Storage' THEN pa.value END) AS storage, " \
                                 "MAX(CASE WHEN a.name = 'Weight' THEN pa.value END) AS weight " \
                                 "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                                 "INNER JOIN products p ON p.id = pa.product_id " \
                                 "INNER JOIN product_price pp ON pp.product_id = p.id " \
                                 "WHERE p.id  != %s GROUP BY p.name, p.rating, pp.base_price, pp.discount_price, " \
                                 "pp.promotion_price"

            query_data_response = "SELECT p.id, p.name,p.img_url, p.rating, pp.base_price, pp.discount_price, pp.promotion_price, " \
                                  "MAX(CASE WHEN a.name = 'CPU' THEN pa.value END) AS cpu, " \
                                  "MAX(CASE WHEN a.name = 'Graphics' THEN pa.value END) AS graphics, " \
                                  "MAX(CASE WHEN a.name = 'RAM' THEN pa.value END) AS ram, " \
                                  "MAX(CASE WHEN a.name = 'Screen' THEN pa.value END) AS screen, " \
                                  "MAX(CASE WHEN a.name = 'Storage' THEN pa.value END) AS storage, " \
                                  "MAX(CASE WHEN a.name = 'Weight' THEN pa.value END) AS weight " \
                                  "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                                  "INNER JOIN products p ON p.id = pa.product_id " \
                                  "INNER JOIN product_price pp ON pp.product_id = p.id " \
                                  "WHERE p.id  IN ({placeholders}) GROUP BY p.name, p.rating, pp.base_price, pp.discount_price, " \
                                  "pp.promotion_price"

            product_info = fetch_product_data(mysql_connection, query_single_product, productId)
            all_products_info = fetch_product_data(mysql_connection, query_all_products, productId)

            product_df = pd.concat([product_info, all_products_info])

            if product_df.empty:
                return jsonify({'error': 'Product not found'}), 404

            product_df = preprocess_data(product_df)

            similar_products = find_similar_products(product_df, knn)

            productIds = similar_products['id'].tolist()
            placeholders = ','.join(['%s' for _ in productIds])

            get_data_response = fetch_product_data(mysql_connection,
                                                   query_data_response.format(placeholders=placeholders), productIds)

            return jsonify(get_data_response.to_dict(orient='records'))

    except Exception as e:
        return jsonify({'error': str(e)}), 500


def fetch_product_data(connection, query, productId):
    with connection.cursor() as cursor:
        cursor.execute(query, productId)
        rows = cursor.fetchall()

    return pd.DataFrame(rows)


def preprocess_data(product_df):
    numeric_fields = ['rating', 'base_price', 'discount_price', 'pr50omotion_price']
    product_df[numeric_fields] = product_df[numeric_fields].astype(int)

    categorical_cols = ['cpu', 'graphics', 'ram', 'screen', 'storage', 'weight']
    product_df[categorical_cols] = product_df[categorical_cols].astype('category').apply(lambda x: x.cat.codes)

    return product_df


def find_similar_products(product_df, knn):
    new_product_info = product_df.drop(columns=['id', 'name'])

    scaler = StandardScaler()
    new_product_info_scaled = scaler.fit_transform(new_product_info)

    distances, indices = knn.kneighbors(new_product_info_scaled)

    similar_products_indices = indices[0]
    similar_products = product_df.iloc[similar_products_indices]

    return similar_products


if __name__ == '__main__':
    app.run(debug=True)
