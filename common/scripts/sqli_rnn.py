# !pip install keras_sequential_ascii
import tensorflow as tf

import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Embedding, SimpleRNN, Dense
from keras.preprocessing.text import Tokenizer
from keras.utils import pad_sequences
from keras.models import load_model

from sklearn.metrics import accuracy_score, classification_report
import numpy as np
from keras_sequential_ascii import keras2ascii

# Load the dataset
dataset_path = 'Modified_SQL_Dataset.csv'
df = pd.read_csv(dataset_path)

# Split the dataset into features (X) and labels (y)
X = df['Query']
y = df['Label']

# Convert labels to numeric values
label_encoder = LabelEncoder()
y = label_encoder.fit_transform(y)

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Tokenize and pad the sequences
max_words = 5000
max_len = 100
tokenizer = Tokenizer(num_words=max_words)
tokenizer.fit_on_texts(X_train)

X_train_seq = tokenizer.texts_to_sequences(X_train)
X_test_seq = tokenizer.texts_to_sequences(X_test)
X_train_padded = pad_sequences(X_train_seq, maxlen=max_len)
X_test_padded = pad_sequences(X_test_seq, maxlen=max_len)

# Build the deep learning model
model = Sequential()
model.add(Embedding(input_dim=max_words, output_dim=128, input_length=max_len))
model.add(SimpleRNN(64))
model.add(Dense(1, activation='sigmoid'))

# Compile the model
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

# Print the model architecture
keras2ascii(model)

# Train the model
model.fit(X_train_padded, y_train, epochs=1, batch_size=32, validation_split=0.2)
# Save model
model.save("sqli_rnn_model.h5")


# Evaluate the model on the test set
y_pred_probs = model.predict(X_test_padded)
y_pred = (y_pred_probs > 0.5).astype(int)  # Convert probabilities to binary predictions

# Convert predictions back to original labels
y_pred_labels = label_encoder.inverse_transform(y_pred.flatten())
y_test_labels = label_encoder.inverse_transform(y_test.flatten())

# Evaluate the model
accuracy = accuracy_score(y_test, y_pred)
classification_rep = classification_report(y_test, y_pred)

print(f"Accuracy: {accuracy}")
print("Classification Report:\n", classification_rep)

# Test queries
test_queries = [
    "SELECT 1/0 from users where username='Ralph';",
    "SELECT * FROM users WHERE username = 'john';",
    "INSERT INTO users (username, password) VALUES ('hacker', 'malicious');",
    "UPDATE products SET price = 1000 WHERE id = 5;",
    "DELETE FROM orders WHERE user_id = 10;",
    "SELECT * FROM users WHERE username = 'john' and 1=1",
    "SELECT id FROM users WHERE username='user' AND password='pass' OR 5=5'",
    "SELECT id, firstname, lastname FROM authors WHERE firstname = 'malicious'ex' and lastname ='newman'"
]

# Convert test queries to NumPy array
test_queries_np = np.array(test_queries)

# Tokenize and pad the sequences
test_sequences = tokenizer.texts_to_sequences(test_queries_np)
test_padded = pad_sequences(test_sequences, maxlen=max_len)
# Load the saved model
loaded_model = load_model("sqli_rnn_model.h5")
# Predictions
test_predictions_probs = loaded_model.predict(test_padded)
test_predictions = (test_predictions_probs > 0.5).astype(int)

# Print results
for query, prediction in zip(test_queries, test_predictions):
    label = "Malicious" if prediction == 1 else "Non-malicious"
    print(f"Query: {query}\nPrediction: {label}\n")
