
swagger_json = {
    "swagger": "2.0",
    "info": {
        "title": "Flask Swagger Demo",
        "version": "1.0",
        "description": "Demo API documentation using Swagger in Flask"
    }
}