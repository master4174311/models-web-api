# Initialize Swagger UI blueprint
SWAGGER_URL = '/api/docs'
API_URL = '/static/swagger.json'
APP_NAME= {'app_name': "Flask Swagger Demo"}
DB_HOST = 'localhost'
DB_USER = 'root'
DB_PASS = '123456'
DB_NAME = 'ecommerce'
DATASET_SQLI_PATH = 'files/Modified_SQL_Dataset.csv'
DATASET_XSS_PATH = 'files/XSS_dataset.csv'
