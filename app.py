from flask import Flask
from flask_restx import Api
from controllers.user_controller import user_namespace
from controllers.product_controller import product_namespace
from controllers.knn_controller import knn_namespace
from controllers.sqli_controller import sqli_namespace
from controllers.xss_controller import xss_namespace
from controllers.models_controller import models_namespace


app = Flask(__name__)

# Initialize the Api object
api = Api(
    app,
    version="1.0",
    title="Your API Title",
    description="Your API Description",
    
)

# Register namespaces for user and product controllers
# api.add_namespace(user_namespace)
# api.add_namespace(product_namespace)
api.add_namespace(models_namespace)

api.add_namespace(knn_namespace)

api.add_namespace(sqli_namespace)
api.add_namespace(xss_namespace)


@app.route('/')
def index():
    return 'Welcome to my Flask API!'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5050,debug=True)
