import pymysql
import sys

class MariaDBConnector:
    def __init__(self, host,port, user, password, database):
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.database = database
        self.connection = None
        self.cursor = None

    def connect(self):
        try:
            self.connection = pymysql.connect(
                host=self.host,
                port=self.port,
                user=self.user,
                password=self.password,
                database=self.database
            )
            self.cursor = self.connection.cursor()
        except pymysql.Error as e:
            print(f"Error connecting to MariaDB: {e}")
            sys.exit(1)

    def disconnect(self):
        if self.connection:
            self.connection.close()
            self.connection = None
            self.cursor = None

    def execute_query(self, query, params=None):
        try:
            if params:
                self.cursor.execute(query, params)

            else:
                self.cursor.execute(query)

            return self.cursor.fetchall()
        except pymysql.Error as e:
            print(f"Error executing query: {e}")
            return None