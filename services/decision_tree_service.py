import os
import joblib
import pandas as pd
import pika
import datetime
import json
from flask import jsonify
from common_service import get_check_ip_vailed, get_lat_long_by_ip_tools
from es_service import push_data_to_elasticsearch, find_lat_long_by_ip
import time
# Define directory and file paths
decision_model_directory = "common/models/"
decision_model_file_path = os.path.join(
decision_model_directory, "decision_tree_model.pkl")

# Access the CLOUDAMQP_URL environment variable and parse it (fallback to localhost)
url = os.environ.get('CLOUDAMQP_URL', 'amqp://huy:NTH@13042000@localhost/')
params = pika.URLParameters(url)
connection = None  # Initialize connection variable

# Define decision function


def decision(data_json):
    # Load the data for prediction
    new_data_predict = pd.DataFrame([data_json])
    # new_data_predict = new_data_predict.drop(new_data_predict.columns[0], axis=1)

    # Load the trained Decision Tree model
    loaded_model = joblib.load('decision_tree_model.pkl')

    # Drop unnecessary columns
    columns_to_drop = ['Source IP', 'Destination IP',
                       'Source Port', 'Protocol', 'MAC Source','MAC Destination','TimeSpan']
    new_data_predict = new_data_predict.drop(columns=columns_to_drop)
    # print(new_data_predict)

    # Add a new column named 'Label' with null values
    new_data_predict['Label'] = None

    # Predict using the trained model
    predicted_labels = loaded_model.predict(
        new_data_predict.drop(columns=['Label']))

    # Assign the predicted labels to the 'Label' column
    new_data_predict['Label'] = predicted_labels

    # Reverse label encoding if needed
    reverse_label_map = {0: 'Normal', 1: 'DDoS', 2: 'DoS Hulk',
                         3: 'DoS GoldenEye', 4: 'DoS slowloris', 5: 'DoS Slowhttptest', }
    new_data_predict['Label'] = new_data_predict['Label'].map(
        reverse_label_map)

    # Load the original data (assuming it's in JSON format)
    data = pd.DataFrame([data_json])
    # Filter rows in the original data with the same 'Destination Port' as new_data_predict and label them as 'Anomaly'
    # anomaly_data = data[data['Destination Port'] ==
    #                     new_data_predict['Destination Port'].iloc[0]]
    # anomaly_data['Label'] = 'Anomaly'
    anomaly_data = data[data['Destination Port']
                        == new_data_predict['Destination Port']]
    anomaly_data['label'] = new_data_predict['Label']

    # Select only the required columns
    # selected_columns = ['Source IP' ,'Destination IP', 'Source Port', 'Destination Port','Protocol', 'TimeSpan', 'Label']
    # anomaly_data = anomaly_data[selected_columns]

    # Return the anomaly data as JSON
    response = anomaly_data.to_json(orient='records')
    return response
# Define dos_process_function


def nomalization_json(data):
    print('Handling nomalization_json()...')
    parsed_data = []

    for d in data:
        parsed_obj = {}
        for k, v in d.items():
            k = k.lower().strip().replace(' ', '_')
            k = k.replace('fwd_packets\\/s', 'fwd_avg_bytes_bulk') \
                 .replace('flow_bytes\\/s', 'flow_bytes_s') \
                 .replace('flow_packets\\/s', 'flow_packets_s') \
                 .replace('bwd_packets\\/s', 'bwd_packets_s') \
                 .replace('down\\/up_ratio', 'down_up_ratio') \
                 .replace('fwd_avg_bytes\\/bulk', 'fwd_avg_bytes_bulk') \
                 .replace('fwd_avg_packets\\/bulk', 'fwd_avg_packets_bulk') \
                 .replace('bwd_avg_bytes\\/bulk', 'bwd_avg_bytes_bulk') \
                 .replace('fwd_header_length.1', 'fwd_header_length_1') \
                 .replace('bwd_avg_packets\\/bulk', 'bwd_avg_packets_bulk')
            parsed_obj[k] = v

        parsed_data.append(parsed_obj)

    print('nomalization_json() completed!')
    for obj in parsed_data:
        obj['creation_time'] = datetime.datetime.now().strftime(
            '%Y-%m-%d %H:%M:%S')
        # try:
        #     ip = obj['source_ip']
        #     if get_check_ip_vailed(ip):
        #         isExist = find_lat_long_by_ip(ip, "anomaly")
        #         if isExist is not None:
        #             obj['location'] = {
        #                 'type': 'Point',
        #                 'coordinates': [isExist[1], isExist[0]]}  # Corrected order for coordinates
        #             obj['location_details'] = {
        #                 'city': isExist[2],
        #                 'region': isExist[3],
        #                 'country': isExist[4]}
        #         else:
        #             print('Case not in ip on elasticsearch')
        #             lat_lon = get_lat_long_by_ip_tools(ip)
        #             if lat_lon is not None:
        #                 obj['location'] = {
        #                     'type': 'Point',
        #                     'coordinates': [lat_lon[1], lat_lon[0]]}  # Corrected order for coordinates
        #                 obj['location_details'] = {
        #                     'city': lat_lon[2],
        #                     'region': lat_lon[3],
        #                     'country': lat_lon[4]}
        #             obj['creation_time'] = datetime.datetime.now().strftime(
        #                 '%Y-%m-%d %H:%M:%S')
        # except Exception as e:
        #     print(f"Error processing IP address: {e}")
        #     obj['creation_time'] = datetime.datetime.now().strftime(
        #         '%Y-%m-%d %H:%M:%S')

    return parsed_data


def dos_process_function(msg):
    print("Dos detection processing")
    data = json.loads(msg)

    # Call the decision function
    predict = decision(data)

    predict_data = json.loads(predict)
    parsed_data = nomalization_json(predict_data)

    print(f"Dos detected {len(parsed_data)}")
    time.sleep(1)  # Simulate processing time
    push_data_to_elasticsearch(parsed_data, "anomaly")

    print("Dos detection processing finished")


def main():
    try:
        # Connect to RabbitMQ
        connection = pika.BlockingConnection(params)
        channel = connection.channel()  # Start a channel
        channel.queue_declare(
            queue='dos_queue', durable=True)  # Declare a queue

        # Define callback function

        def callback(ch, method, properties, body):

            # Use asyncio.create_task() to schedule the coroutine
            dos_process_function(body)

        # Set up subscription on the queue
        channel.basic_consume('dos_queue', callback, auto_ack=True)

        print('Waiting for messages. To exit press CTRL+C')

        # Start consuming (blocks)
        channel.start_consuming()
    except pika.exceptions.AMQPConnectionError:
        print("Failed to connect to RabbitMQ")
    except KeyboardInterrupt:
        print("Consumer stopped by user")
    finally:
        # Close connection
        if connection and connection.is_open:
            connection.close()


# nest_asyncio.apply(main())
main()
