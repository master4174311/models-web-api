import tensorflow as tf
import os
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Embedding, SimpleRNN, Dense
from keras.preprocessing.text import Tokenizer
from keras.utils import pad_sequences
from keras.models import load_model
from sklearn.metrics import f1_score
from flask import jsonify, request
from keras.preprocessing.text import Tokenizer

from sklearn.metrics import accuracy_score, classification_report
import numpy as np
from keras_sequential_ascii import keras2ascii
from  common.constants.constants import *

# Define directory and file paths
xss_model_directory = "common/models/"
xss_model_file_path = os.path.join(xss_model_directory, "xss_rnn_model.h5")




def train_xss_model(max_words=5000, max_len=100, test_size=0.2, random_state=42,epochs=5,batch_size=32,validation_split=0.2,output_dim=128,unitRNN=64,unitDense=1):
    try:
        if os.path.exists(xss_model_file_path):
            return {'message': 'Model already trained. No need to train again.'}, 200
        else:
            df = pd.read_csv(DATASET_XSS_PATH)
            X = df['Sentence']
            y = df['Label']
            
            # Convert labels to numeric values
            label_encoder = LabelEncoder()
            y = label_encoder.fit_transform(y)
            
            # Split the data into training and testing sets
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=random_state)
            
            # Tokenize and pad the sequences
            tokenizer = Tokenizer(num_words=max_words)
            tokenizer.fit_on_texts(X_train)
            X_train_seq = tokenizer.texts_to_sequences(X_train)
            X_test_seq = tokenizer.texts_to_sequences(X_test)
            X_train_padded = pad_sequences(X_train_seq, maxlen=max_len)
            X_test_padded = pad_sequences(X_test_seq, maxlen=max_len)
            
            # Build the deep learning model
            model = Sequential()
            model.add(Embedding(input_dim=max_words, output_dim=output_dim, input_length=max_len))
            model.add(SimpleRNN(unitRNN))
            model.add(Dense(unitDense, activation='sigmoid'))
            
            # Compile the model
            model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
            
            # Print the model architecture
            keras2ascii(model)
            
            # Train the model
            model.fit(X_train_padded, y_train, epochs=epochs, batch_size=batch_size, validation_split=validation_split)
            
            # Evaluate the model on the test data
            y_pred = model.predict_classes(X_test_padded)
            f1 = f1_score(y_test, y_pred)
            
            # Save model
            model.save(xss_model_file_path)

            return {'message': 'Model trained successfully', 'accuracy': f1}, 200
    except Exception as e:
        return {'error': str(e)}, 500

def retrain_xss_model(max_words=5000, max_len=100, test_size=0.2, random_state=42,epochs=5,batch_size=32,validation_split=0.2,output_dim=128,unitRNN=64,unitDense=1):
    try:
        if os.path.exists(xss_model_file_path):
            os.remove(xss_model_file_path)
            df = pd.read_csv(DATASET_XSS_PATH)
            X = df['Sentence']
            y = df['Label']
            label_encoder = LabelEncoder()
            y = label_encoder.fit_transform(y)
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=random_state)

            # Tokenize and pad the sequences
            max_words = max_words
            max_len = max_len
            tokenizer = Tokenizer(num_words=max_words)
            tokenizer.fit_on_texts(X_train)

            X_train_seq = tokenizer.texts_to_sequences(X_train)
            X_test_seq = tokenizer.texts_to_sequences(X_test)
            X_train_padded = pad_sequences(X_train_seq, maxlen=max_len)
            X_test_padded = pad_sequences(X_test_seq, maxlen=max_len)

            # Build the deep learning model
            model = Sequential()
            model.add(Embedding(input_dim=max_words, output_dim=output_dim, input_length=max_len))
            model.add(SimpleRNN(unitRNN))
            model.add(Dense(unitDense, activation='sigmoid'))

            # Compile the model
            model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
            model.fit(X_train_padded, y_train, epochs=epochs, batch_size=batch_size, validation_split=validation_split)

            # Evaluate the model on the test data
            y_pred = model.predict_classes(X_test_padded)
            f1 = f1_score(y_test, y_pred)

            # Save model
            model.save(xss_model_file_path)

            return {'message': 'Model retrained successfully', 'accuracy': f1}, 200
        else:
            train_xss_model(max_words, max_len, test_size, random_state,epochs,batch_size,validation_split,output_dim,unitRNN,unitDense)
            return {'message': 'Model retrained successfully', 'accuracy': f1}, 200
            # return {'error': 'Model not found. Please train the model first.'}, 404
    except Exception as e:
        return {'error': str(e)}, 500

def check_xss(test_queries, max_len, max_words):
    try:
        if os.path.exists(xss_model_file_path):
            test_queries_np = np.array(test_queries).astype(str)
            tokenizer = Tokenizer(num_words=max_words)
            tokenizer.fit_on_texts(test_queries_np)

            test_sequences = tokenizer.texts_to_sequences(test_queries_np)
            test_padded = pad_sequences(test_sequences, maxlen=max_len)

            loaded_model = load_model(xss_model_file_path)

            test_predictions_probs = loaded_model.predict(test_padded)
            test_predictions = (test_predictions_probs > 0.5).astype(int)

            results = []
            for query, prediction in zip(test_queries, test_predictions):
                is_safe = 1 if prediction == 0 else 0
                name = "Non-malicious" if prediction == 0 else "Malicious"
                results.append({'query': query, 'isSafe': is_safe, 'name': name})

            return results
        else:
            return {'error': 'Model not found. Please train the model first.'}, 404
    except Exception as e:
        return {'error': str(e)}, 500


