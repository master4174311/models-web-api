
import pandas as pd
import time
import random
from flask import jsonify
from common_service import get_check_ip_vailed, get_lat_long_by_ip_tools
from es_service import find_list_ip_anomaly, update_lat_lon_location, find_lat_long_by_ip


def update():

    doc = find_list_ip_anomaly("anomaly")
    if doc is not None:
        for ip in doc:
            ip = ip['key']
            isCheckVailed = get_check_ip_vailed(ip)
            isExist = find_lat_long_by_ip(ip, "anomaly")
            if isExist is not None:
                print(f'Is Exist {isExist}')
                continue
            else:
                if isCheckVailed:
                    data = get_lat_long_by_ip_tools(ip)
                    if data is not None:
                        updateData = update_lat_lon_location(
                            ip, [data[0], data[1]], [data[2], data[3], data[4]], 'anomaly')
                        print(f'Updated ip {ip} - status: {updateData}')
                        x = random.randint(1, 5)
                        time.sleep(x)
    else:
        print('Do not data update')


def main():
    print('hello')
    update()


# nest_asyncio.apply(main())
main()
