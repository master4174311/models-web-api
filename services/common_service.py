import json
import requests
from ip2geotools.databases.noncommercial import DbIpCity
from geopy.distance import distance
import re

import io

import os
import pandas as pd

from flask import jsonify

regex = "^((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])$"
def get_check_ip_vailed(Ip): 
 
    # pass the regular expression
    # and the string in search() method
    if(re.search(regex, Ip)): 
        return True
         
    else: 
        return False
def get_lat_long_by_ip_tools(ip):
    print('Handling get_lat_long_by_ip_tools()...')

    try:

        res =  DbIpCity.get(ip, api_key="free")

        return [res.latitude, res.longitude,res.city,res.region,res.country]
    except Exception as e:
        print(f"Error get_lat_long_by_ip_tools: {e}")
        return None

def get_lat_lon_by_geolocation(ip):
    request_url = f'https://ipinfo.io/widget/demo/{ip}'
    response = requests.get(request_url)
    result = response.content.decode()
    print(result)
    
def append_prediction_to_csv(predict):
    # Convert JSON string to DataFrame
    predict_df = pd.read_json(predict)

    # Define the path to the existing CSV file
    output_file = os.path.join('../files', "prediction.csv")

    # Check if the CSV file exists
    if os.path.exists(output_file):
        # Read the existing CSV file
        existing_df = pd.read_csv(output_file)

        # Concatenate the existing DataFrame with the new prediction DataFrame
        combined_df = pd.concat([existing_df, predict_df], ignore_index=True)
    else:
        # If the CSV file doesn't exist, use the new prediction DataFrame
        combined_df = predict_df

    # Save the combined DataFrame to CSV
    combined_df.to_csv(output_file, index=False)


# rs = get_lat_lon_by_geolocation('108.162.246.43')
# print(rs)