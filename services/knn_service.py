import os
import joblib
import pandas as pd
import pymysql
from sklearn.neighbors import NearestNeighbors
from sklearn.preprocessing import StandardScaler
from flask import jsonify
from common.constants.constants import *

# Define directory and file paths
knn_model_directory = "common/models/"
knn_model_file_path = os.path.join(knn_model_directory, "knn_model.pkl")
knn_model_graphics_file_path = os.path.join(
    knn_model_directory, "knn_model_graphics.pkl")
knn_model_storage_file_path = os.path.join(
    knn_model_directory, "knn_model_storage.pkl")
knn_model_ram_file_path = os.path.join(
    knn_model_directory, "knn_model_ram.pkl")
knn_model_screen_file_path = os.path.join(
    knn_model_directory, "knn_model_screen.pkl")
knn_model_weight_file_path = os.path.join(
    knn_model_directory, "knn_model_weight.pkl")
knn_model_cpu_file_path = os.path.join(
    knn_model_directory, "knn_model_cpu.pkl")
knn_model_price_file_path = os.path.join(
    knn_model_directory, "knn_model_price.pkl")

def train_knn_model(k):
    try:
        if os.path.exists(knn_model_file_path):
            return {'message': 'Model already trained. No need to train again.'}, 200
        else:
            # Connect to the database
            with pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASS, db=DB_NAME, charset='utf8',
                                 cursorclass=pymysql.cursors.DictCursor) as mysql_connection:

                query_all_products = "SELECT p.id, p.name, p.rating," \
                    "MAX(CASE WHEN a.name = 'CPU' THEN pa.value END) AS cpu, " \
                    "MAX(CASE WHEN a.name = 'Graphics' THEN pa.value END) AS graphics, " \
                    "MAX(CASE WHEN a.name = 'RAM' THEN pa.value END) AS ram, " \
                    "MAX(CASE WHEN a.name = 'Screen' THEN pa.value END) AS screen, " \
                    "MAX(CASE WHEN a.name = 'Storage' THEN pa.value END) AS storage, " \
                    "MAX(CASE WHEN a.name = 'Weight' THEN pa.value END) AS weight " \
                    "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                    "INNER JOIN products p ON p.id = pa.product_id " \
                    "INNER JOIN product_price pp ON pp.product_id = p.id " \
                    "GROUP BY p.name, p.rating"
                df = fetch_all_product_data(
                    mysql_connection, query_all_products)
                # Encode categorical data
                categorical_cols = ['cpu', 'graphics',
                                    'ram', 'screen', 'storage', 'weight']
                df[categorical_cols] = df[categorical_cols].astype('category')
                df[categorical_cols] = df[categorical_cols].apply(
                    lambda x: x.cat.codes)
                # Prepare input data for KNN
                X = df[['rating', 'cpu', 'graphics',
                        'ram', 'screen', 'storage', 'weight']]
                scaler = StandardScaler()
                X_scaled = scaler.fit_transform(X)
                k = int(k)
                # Train KNN model
                knn = NearestNeighbors(n_neighbors=k, algorithm='auto')
                knn.fit(X_scaled)
                # Save trained model
                joblib.dump(knn, knn_model_file_path)

                return {'message': f'Model trained successfully with k_neighbors: {k}'}, 200
    except Exception as e:
        return {'error': str(e)}, 500


def retrain_knn_model(k):
    try:
        if os.path.exists(knn_model_file_path):
            # Load data from the database for retraining
            with pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASS, db=DB_NAME, charset='utf8',
                                 cursorclass=pymysql.cursors.DictCursor) as mysql_connection:
                query_all_products = "SELECT p.id, p.name, p.rating, " \
                    "MAX(CASE WHEN a.name = 'CPU' THEN pa.value END) AS cpu, " \
                    "MAX(CASE WHEN a.name = 'Graphics' THEN pa.value END) AS graphics, " \
                    "MAX(CASE WHEN a.name = 'RAM' THEN pa.value END) AS ram, " \
                    "MAX(CASE WHEN a.name = 'Screen' THEN pa.value END) AS screen, " \
                    "MAX(CASE WHEN a.name = 'Storage' THEN pa.value END) AS storage, " \
                    "MAX(CASE WHEN a.name = 'Weight' THEN pa.value END) AS weight " \
                    "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                    "INNER JOIN products p ON p.id = pa.product_id " \
                    "INNER JOIN product_price pp ON pp.product_id = p.id " \
                    "GROUP BY p.name, p.rating"
                updated_df = fetch_all_product_data(
                    mysql_connection, query_all_products)

            # Encode categorical data
            categorical_cols = ['cpu', 'graphics',
                                'ram', 'screen', 'storage', 'weight']
            updated_df[categorical_cols] = updated_df[categorical_cols].astype(
                'category')
            updated_df[categorical_cols] = updated_df[categorical_cols].apply(
                lambda x: x.cat.codes)

            # Prepare input data for KNN
            X_updated = updated_df[[
                'rating', 'cpu', 'graphics', 'ram', 'screen', 'storage', 'weight']]
            scaler_updated = StandardScaler()
            X_scaled_updated = scaler_updated.fit_transform(X_updated)
            k_neighbors = int(k)

            # Retrain KNN model
            knn_updated = NearestNeighbors(
                n_neighbors=k_neighbors, algorithm='auto')
            knn_updated.fit(X_scaled_updated)

            # Save retrained model
            joblib.dump(knn_updated, knn_model_file_path)
            return jsonify({'message': f'Model retrained successfully with k: {k_neighbors}'})
        else:
            return {'error': 'Model not found. Please train the model first.'}, 404
    except Exception as e:
        return {'error': str(e)}, 500


def similar(productId):
    if os.path.exists(knn_model_file_path):
        # Load KNN model
        knn = joblib.load(knn_model_file_path)

        # Load data from the database
    with pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASS, db=DB_NAME, charset='utf8',
                         cursorclass=pymysql.cursors.DictCursor) as mysql_connection:
        query_single_product = "SELECT p.id, p.name, p.rating, " \
                               "MAX(CASE WHEN a.name = 'CPU' THEN pa.value END) AS cpu, " \
                               "MAX(CASE WHEN a.name = 'Graphics' THEN pa.value END) AS graphics, " \
                               "MAX(CASE WHEN a.name = 'RAM' THEN pa.value END) AS ram, " \
                               "MAX(CASE WHEN a.name = 'Screen' THEN pa.value END) AS screen, " \
                               "MAX(CASE WHEN a.name = 'Storage' THEN pa.value END) AS storage, " \
                               "MAX(CASE WHEN a.name = 'Weight' THEN pa.value END) AS weight " \
                               "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                               "INNER JOIN products p ON p.id = pa.product_id " \
                               "INNER JOIN product_price pp ON pp.product_id = p.id " \
                               "WHERE p.id  = %s GROUP BY p.id, p.name"

        query_all_products = "SELECT p.id, p.name, p.rating," \
                             "MAX(CASE WHEN a.name = 'CPU' THEN pa.value END) AS cpu, " \
                             "MAX(CASE WHEN a.name = 'Graphics' THEN pa.value END) AS graphics, " \
                             "MAX(CASE WHEN a.name = 'RAM' THEN pa.value END) AS ram, " \
                             "MAX(CASE WHEN a.name = 'Screen' THEN pa.value END) AS screen, " \
                             "MAX(CASE WHEN a.name = 'Storage' THEN pa.value END) AS storage, " \
                             "MAX(CASE WHEN a.name = 'Weight' THEN pa.value END) AS weight " \
                             "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                             "INNER JOIN products p ON p.id = pa.product_id " \
                             "INNER JOIN product_price pp ON pp.product_id = p.id " \
                             "WHERE p.id  != %s GROUP BY p.name, p.rating"

        query_data_response = "SELECT p.id, p.name,p.img_url, p.rating," \
                              "MAX(CASE WHEN a.name = 'CPU' THEN pa.value END) AS cpu, " \
                              "MAX(CASE WHEN a.name = 'Graphics' THEN pa.value END) AS graphics, " \
                              "MAX(CASE WHEN a.name = 'RAM' THEN pa.value END) AS ram, " \
                              "MAX(CASE WHEN a.name = 'Screen' THEN pa.value END) AS screen, " \
                              "MAX(CASE WHEN a.name = 'Storage' THEN pa.value END) AS storage, " \
                              "MAX(CASE WHEN a.name = 'Weight' THEN pa.value END) AS weight " \
                              "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                              "INNER JOIN products p ON p.id = pa.product_id " \
                              "INNER JOIN product_price pp ON pp.product_id = p.id " \
                              "WHERE p.id  IN ({placeholders}) GROUP BY p.name"

        product_info = fetch_product_data(
            mysql_connection, query_single_product, productId)
        all_products_info = fetch_product_data(
            mysql_connection, query_all_products, productId)

        product_df = pd.concat([product_info, all_products_info])

        if product_df.empty:
            return jsonify({'error': 'Product not found'}), 404

        product_df = preprocess_data(product_df)

        similar_products = find_similar_products(product_df, knn)

        productIds = similar_products['id'].tolist()
        placeholders = ','.join(['%s' for _ in productIds])

        get_data_response = fetch_product_data(mysql_connection,
                                               query_data_response.format(placeholders=placeholders), productIds)

        return jsonify(get_data_response.to_dict(orient='records'))


def similar_cpu(productId):
    if os.path.exists(knn_model_cpu_file_path):
        # Load KNN model
        knn = joblib.load(knn_model_cpu_file_path)

        # Load data from the database
    with pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASS, db=DB_NAME, charset='utf8',
                         cursorclass=pymysql.cursors.DictCursor) as mysql_connection:
        query_single_product = "SELECT p.id, p.name, " \
                               "MAX(CASE WHEN a.name = 'CPU' THEN pa.value END) AS cpu " \
                               "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                               "INNER JOIN products p ON p.id = pa.product_id " \
                               "INNER JOIN product_price pp ON pp.product_id = p.id " \
                               "WHERE p.id  = %s GROUP BY p.id, p.name"
        query_top1_single_product = "SELECT p.id, p.name, " \
                               "MAX(CASE WHEN a.name = 'CPU' THEN pa.value END) AS cpu " \
                               "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                               "INNER JOIN products p ON p.id = pa.product_id " \
                               "INNER JOIN product_price pp ON pp.product_id = p.id " \
                               "GROUP BY p.id, p.name" \
                               "LIMIT 1"
        query_all_products = "SELECT p.id, p.name," \
                             "MAX(CASE WHEN a.name = 'CPU' THEN pa.value END) AS cpu " \
                             "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                             "INNER JOIN products p ON p.id = pa.product_id " \
                             "INNER JOIN product_price pp ON pp.product_id = p.id " \
                             "WHERE p.id  != %s GROUP BY p.name"

        query_data_response = "SELECT p.id, p.name,p.img_url," \
                              "MAX(CASE WHEN a.name = 'CPU' THEN pa.value END) AS cpu " \
                              "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                              "INNER JOIN products p ON p.id = pa.product_id " \
                              "INNER JOIN product_price pp ON pp.product_id = p.id " \
                              "WHERE p.id  IN ({placeholders}) GROUP BY p.name"
        if productId is not None or productId is "":
            product_info = fetch_product_data(
                mysql_connection, query_single_product, productId)
        else:
            product_info = fetch_product_data(
            mysql_connection, query_top1_single_product, productId)
            
            
        all_products_info = fetch_product_data(
            mysql_connection, query_all_products, productId)

        product_df = pd.concat([product_info, all_products_info])

        if product_df.empty:
            return jsonify({'error': 'Product not found'}), 404

        product_df = preprocess_data_by_params(product_df, 'cpu')

        similar_products = find_similar_products(product_df, knn)
        print(similar_products)
        productIds = similar_products['id'].tolist()
        placeholders = ','.join(['%s' for _ in productIds])

        get_data_response = fetch_product_data(mysql_connection,
                                               query_data_response.format(placeholders=placeholders), productIds)

        return jsonify(get_data_response.to_dict(orient='records'))


def similar_weight(productId):
    if os.path.exists(knn_model_weight_file_path):
        # Load KNN model
        knn = joblib.load(knn_model_weight_file_path)

        # Load data from the database
    with pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASS, db=DB_NAME, charset='utf8',
                         cursorclass=pymysql.cursors.DictCursor) as mysql_connection:
        query_single_product = "SELECT p.id, p.name, " \
                               "MAX(CASE WHEN a.name = 'Weight' THEN pa.value END) AS weight  " \
                               "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                               "INNER JOIN products p ON p.id = pa.product_id " \
                               "INNER JOIN product_price pp ON pp.product_id = p.id " \
                               "WHERE p.id  = %s GROUP BY p.id, p.name"

        query_all_products = "SELECT p.id, p.name," \
                             "MAX(CASE WHEN a.name = 'Weight' THEN pa.value END) AS weight " \
                             "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                             "INNER JOIN products p ON p.id = pa.product_id " \
                             "INNER JOIN product_price pp ON pp.product_id = p.id " \
                             "WHERE p.id  != %s GROUP BY p.name"

        query_data_response = "SELECT p.id, p.name,p.img_url," \
                              "MAX(CASE WHEN a.name = 'Weight' THEN pa.value END) AS weight  " \
                              "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                              "INNER JOIN products p ON p.id = pa.product_id " \
                              "INNER JOIN product_price pp ON pp.product_id = p.id " \
                              "WHERE p.id  IN ({placeholders}) GROUP BY p.name"

        query_top1_single_product = "SELECT p.id, p.name, " \
                               "MAX(CASE WHEN a.name = 'Weight' THEN pa.value END) AS weight " \
                               "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                               "INNER JOIN products p ON p.id = pa.product_id " \
                               "INNER JOIN product_price pp ON pp.product_id = p.id " \
                               "GROUP BY p.id, p.name" \
                               "LIMIT 1"


        if productId is not None or productId is "":
            product_info = fetch_product_data(
                mysql_connection, query_single_product, productId)
        else:
            product_info = fetch_product_data(
            mysql_connection, query_top1_single_product, productId)
            
        all_products_info = fetch_product_data(
            mysql_connection, query_all_products, productId)

        product_df = pd.concat([product_info, all_products_info])

        if product_df.empty:
            return jsonify({'error': 'Product not found'}), 404

        product_df = preprocess_data_by_params(product_df, 'weight')

        similar_products = find_similar_products(product_df, knn)
        print(similar_products)
        productIds = similar_products['id'].tolist()
        placeholders = ','.join(['%s' for _ in productIds])

        get_data_response = fetch_product_data(mysql_connection,
                                               query_data_response.format(placeholders=placeholders), productIds)

        return jsonify(get_data_response.to_dict(orient='records'))


def similar_screen(productId):
    if os.path.exists(knn_model_screen_file_path):
        # Load KNN model
        knn = joblib.load(knn_model_weight_file_path)

        # Load data from the database
    with pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASS, db=DB_NAME, charset='utf8',
                         cursorclass=pymysql.cursors.DictCursor) as mysql_connection:
        query_single_product = "SELECT p.id, p.name, " \
                               "MAX(CASE WHEN a.name = 'Screen' THEN pa.value END) AS screen  " \
                               "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                               "INNER JOIN products p ON p.id = pa.product_id " \
                               "INNER JOIN product_price pp ON pp.product_id = p.id " \
                               "WHERE p.id  = %s GROUP BY p.id, p.name"

        query_all_products = "SELECT p.id, p.name," \
                             "MAX(CASE WHEN a.name = 'Screen' THEN pa.value END) AS screen " \
                             "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                             "INNER JOIN products p ON p.id = pa.product_id " \
                             "INNER JOIN product_price pp ON pp.product_id = p.id " \
                             "WHERE p.id  != %s GROUP BY p.name"

        query_data_response = "SELECT p.id, p.name,p.img_url," \
                              "MAX(CASE WHEN a.name = 'Screen' THEN pa.value END) AS screen  " \
                              "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                              "INNER JOIN products p ON p.id = pa.product_id " \
                              "INNER JOIN product_price pp ON pp.product_id = p.id " \
                              "WHERE p.id  IN ({placeholders}) GROUP BY p.name"

        query_top1_single_product = "SELECT p.id, p.name, " \
                               "MAX(CASE WHEN a.name = 'Screen' THEN pa.value END) AS screen " \
                               "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                               "INNER JOIN products p ON p.id = pa.product_id " \
                               "INNER JOIN product_price pp ON pp.product_id = p.id " \
                               "GROUP BY p.id, p.name" \
                               "LIMIT 1"


        if productId is not None or productId is "":
            product_info = fetch_product_data(
                mysql_connection, query_single_product, productId)
        else:
            product_info = fetch_product_data(
            mysql_connection, query_top1_single_product, productId)

        all_products_info = fetch_product_data(
            mysql_connection, query_all_products, productId)

        product_df = pd.concat([product_info, all_products_info])

        if product_df.empty:
            return jsonify({'error': 'Product not found'}), 404

        product_df = preprocess_data_by_params(product_df, 'screen')

        similar_products = find_similar_products(product_df, knn)
        print(similar_products)
        productIds = similar_products['id'].tolist()
        placeholders = ','.join(['%s' for _ in productIds])

        get_data_response = fetch_product_data(mysql_connection,
                                               query_data_response.format(placeholders=placeholders), productIds)

        return jsonify(get_data_response.to_dict(orient='records'))


def similar_ram(productId):
    if os.path.exists(knn_model_ram_file_path):
        # Load KNN model
        knn = joblib.load(knn_model_weight_file_path)

        # Load data from the database
    with pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASS, db=DB_NAME, charset='utf8',
                         cursorclass=pymysql.cursors.DictCursor) as mysql_connection:
        query_single_product = "SELECT p.id, p.name, " \
                               "MAX(CASE WHEN a.name = 'RAM' THEN pa.value END) AS ram " \
                               "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                               "INNER JOIN products p ON p.id = pa.product_id " \
                               "INNER JOIN product_price pp ON pp.product_id = p.id " \
                               "WHERE p.id  = %s GROUP BY p.id, p.name"

        query_all_products = "SELECT p.id, p.name," \
                             "MAX(CASE WHEN a.name = 'RAM' THEN pa.value END) AS ram " \
                             "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                             "INNER JOIN products p ON p.id = pa.product_id " \
                             "INNER JOIN product_price pp ON pp.product_id = p.id " \
                             "WHERE p.id  != %s GROUP BY p.name"

        query_data_response = "SELECT p.id, p.name,p.img_url," \
                              "MAX(CASE WHEN a.name = 'RAM' THEN pa.value END) AS ram  " \
                              "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                              "INNER JOIN products p ON p.id = pa.product_id " \
                              "INNER JOIN product_price pp ON pp.product_id = p.id " \
                              "WHERE p.id  IN ({placeholders}) GROUP BY p.name"


        query_top1_single_product = "SELECT p.id, p.name, " \
                               "MAX(CASE WHEN a.name = 'RAM' THEN pa.value END) AS ram " \
                               "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                               "INNER JOIN products p ON p.id = pa.product_id " \
                               "INNER JOIN product_price pp ON pp.product_id = p.id " \
                               "GROUP BY p.id, p.name" \
                               "LIMIT 1"


        if productId is not None or productId is "":
            product_info = fetch_product_data(
                mysql_connection, query_single_product, productId)
        else:
            product_info = fetch_product_data(
            mysql_connection, query_top1_single_product, productId)
            
        all_products_info = fetch_product_data(
            mysql_connection, query_all_products, productId)

        product_df = pd.concat([product_info, all_products_info])

        if product_df.empty:
            return jsonify({'error': 'Product not found'}), 404

        product_df = preprocess_data_by_params(product_df, 'ram')

        similar_products = find_similar_products(product_df, knn)
        print(similar_products)
        productIds = similar_products['id'].tolist()
        placeholders = ','.join(['%s' for _ in productIds])

        get_data_response = fetch_product_data(mysql_connection,
                                               query_data_response.format(placeholders=placeholders), productIds)

        return jsonify(get_data_response.to_dict(orient='records'))


def similar_storage(productId):
    if os.path.exists(knn_model_storage_file_path):
        # Load KNN model
        knn = joblib.load(knn_model_weight_file_path)

        # Load data from the database
    with pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASS, db=DB_NAME, charset='utf8',
                         cursorclass=pymysql.cursors.DictCursor) as mysql_connection:
        query_single_product = "SELECT p.id, p.name, " \
                               "MAX(CASE WHEN a.name = 'Storage' THEN pa.value END) AS storage " \
                               "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                               "INNER JOIN products p ON p.id = pa.product_id " \
                               "INNER JOIN product_price pp ON pp.product_id = p.id " \
                               "WHERE p.id  = %s GROUP BY p.id, p.name"

        query_all_products = "SELECT p.id, p.name," \
                             "MAX(CASE WHEN a.name = 'Storage' THEN pa.value END) AS storage " \
                             "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                             "INNER JOIN products p ON p.id = pa.product_id " \
                             "INNER JOIN product_price pp ON pp.product_id = p.id " \
                             "WHERE p.id  != %s GROUP BY p.name"

        query_data_response = "SELECT p.id, p.name,p.img_url," \
                              "MAX(CASE WHEN a.name = 'Storage' THEN pa.value END) AS storage  " \
                              "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                              "INNER JOIN products p ON p.id = pa.product_id " \
                              "INNER JOIN product_price pp ON pp.product_id = p.id " \
                              "WHERE p.id  IN ({placeholders}) GROUP BY p.name"

        query_top1_single_product = "SELECT p.id, p.name, " \
                               "MAX(CASE WHEN a.name = 'Storage' THEN pa.value END) AS storage " \
                               "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                               "INNER JOIN products p ON p.id = pa.product_id " \
                               "INNER JOIN product_price pp ON pp.product_id = p.id " \
                               "GROUP BY p.id, p.name" \
                               "LIMIT 1"


        if productId is not None or productId is "":
            product_info = fetch_product_data(
                mysql_connection, query_single_product, productId)
        else:
            product_info = fetch_product_data(
            mysql_connection, query_top1_single_product, productId)

        
        all_products_info = fetch_product_data(
            mysql_connection, query_all_products, productId)

        product_df = pd.concat([product_info, all_products_info])

        if product_df.empty:
            return jsonify({'error': 'Product not found'}), 404

        product_df = preprocess_data_by_params(product_df, 'storage')

        similar_products = find_similar_products(product_df, knn)
        print(similar_products)
        productIds = similar_products['id'].tolist()
        placeholders = ','.join(['%s' for _ in productIds])

        get_data_response = fetch_product_data(mysql_connection,
                                               query_data_response.format(placeholders=placeholders), productIds)

        return jsonify(get_data_response.to_dict(orient='records'))


def similar_graphics(productId):
    if os.path.exists(knn_model_graphics_file_path):
        # Load KNN model
        knn = joblib.load(knn_model_weight_file_path)

        # Load data from the database
    with pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASS, db=DB_NAME, charset='utf8',
                         cursorclass=pymysql.cursors.DictCursor) as mysql_connection:
        query_single_product = "SELECT p.id, p.name, " \
                               "MAX(CASE WHEN a.name = 'Graphics' THEN pa.value END) AS graphics " \
                               "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                               "INNER JOIN products p ON p.id = pa.product_id " \
                               "INNER JOIN product_price pp ON pp.product_id = p.id " \
                               "WHERE p.id  = %s GROUP BY p.id, p.name"

        query_all_products = "SELECT p.id, p.name," \
                             "MAX(CASE WHEN a.name = 'Graphics' THEN pa.value END) AS graphics " \
                             "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                             "INNER JOIN products p ON p.id = pa.product_id " \
                             "INNER JOIN product_price pp ON pp.product_id = p.id " \
                             "WHERE p.id  != %s GROUP BY p.name"

        query_data_response = "SELECT p.id, p.name,p.img_url," \
                              "MAX(CASE WHEN a.name = 'Graphics' THEN pa.value END) AS graphics  " \
                              "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                              "INNER JOIN products p ON p.id = pa.product_id " \
                              "INNER JOIN product_price pp ON pp.product_id = p.id " \
                              "WHERE p.id  IN ({placeholders}) GROUP BY p.name"

        query_top1_single_product = "SELECT p.id, p.name, " \
                               "MAX(CASE WHEN a.name = 'Graphics' THEN pa.value END) AS graphics" \
                               "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                               "INNER JOIN products p ON p.id = pa.product_id " \
                               "INNER JOIN product_price pp ON pp.product_id = p.id " \
                               "GROUP BY p.id, p.name" \
                               "LIMIT 1"


        if productId is not None or productId is "":
            product_info = fetch_product_data(
                mysql_connection, query_single_product, productId)
        else:
            product_info = fetch_product_data(
            mysql_connection, query_top1_single_product, productId)

        
        
        all_products_info = fetch_product_data(
            mysql_connection, query_all_products, productId)

        product_df = pd.concat([product_info, all_products_info])

        if product_df.empty:
            return jsonify({'error': 'Product not found'}), 404

        product_df = preprocess_data_by_params(product_df, 'graphics')

        similar_products = find_similar_products(product_df, knn)
        print(similar_products)
        productIds = similar_products['id'].tolist()
        placeholders = ','.join(['%s' for _ in productIds])

        get_data_response = fetch_product_data(mysql_connection,
                                               query_data_response.format(placeholders=placeholders), productIds)

        return jsonify(get_data_response.to_dict(orient='records'))

def similar_price(productId):
    if os.path.exists(knn_model_price_file_path):
        # Load KNN model
        knn = joblib.load(knn_model_price_file_path)

        # Load data from the database
    with pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASS, db=DB_NAME, charset='utf8',
                         cursorclass=pymysql.cursors.DictCursor) as mysql_connection:
        query_single_product = "SELECT p.id, p.name,p.price " \
                               "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                               "INNER JOIN products p ON p.id = pa.product_id " \
                               "INNER JOIN product_price pp ON pp.product_id = p.id " \
                               "WHERE p.id  = %s GROUP BY p.id, p.name"

        query_all_products = "SELECT p.id, p.name,p.price" \
                             "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                             "INNER JOIN products p ON p.id = pa.product_id " \
                             "INNER JOIN product_price pp ON pp.product_id = p.id " \
                             "WHERE p.id  != %s GROUP BY p.name"

        query_data_response = "SELECT p.id, p.name,p.img_url," \
                              "MAX(CASE WHEN a.name = 'Graphics' THEN pa.value END) AS graphics  " \
                              "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                              "INNER JOIN products p ON p.id = pa.product_id " \
                              "INNER JOIN product_price pp ON pp.product_id = p.id " \
                              "WHERE p.id  IN ({placeholders}) GROUP BY p.name"

        product_info = fetch_product_data(
            mysql_connection, query_single_product, productId)
        all_products_info = fetch_product_data(
            mysql_connection, query_all_products, productId)

        product_df = pd.concat([product_info, all_products_info])

        if product_df.empty:
            return jsonify({'error': 'Product not found'}), 404

        product_df = preprocess_data_by_params(product_df, 'price')

        similar_products = find_similar_products(product_df, knn)
        print(similar_products)
        productIds = similar_products['id'].tolist()
        placeholders = ','.join(['%s' for _ in productIds])

        get_data_response = fetch_product_data(mysql_connection,
                                               query_data_response.format(placeholders=placeholders), productIds)

        return jsonify(get_data_response.to_dict(orient='records'))

def retrain_knn_model_by_params_cpu(k):
    try:
        # Load data from the database for retraining
        with pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASS, db=DB_NAME, charset='utf8',
                             cursorclass=pymysql.cursors.DictCursor) as mysql_connection:
            query_all_products = "SELECT p.id, p.name," \
                "MAX(CASE WHEN a.name = 'CPU' THEN pa.value END) AS cpu " \
                "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                "INNER JOIN products p ON p.id = pa.product_id " \
                "INNER JOIN product_price pp ON pp.product_id = p.id " \
                "GROUP BY p.name"
            updated_df = fetch_all_product_data(
                mysql_connection, query_all_products)

        # Encode categorical data
        categorical_cols = ['cpu']
        updated_df[categorical_cols] = updated_df[categorical_cols].astype(
            'category')
        updated_df[categorical_cols] = updated_df[categorical_cols].apply(
            lambda x: x.cat.codes)

        # Prepare input data for KNN
        X_updated = updated_df[['cpu']]
        scaler_updated = StandardScaler()
        X_scaled_updated = scaler_updated.fit_transform(X_updated)
        k_neighbors = int(k)

        # Retrain KNN model
        knn_updated = NearestNeighbors(
            n_neighbors=k_neighbors, algorithm='auto')
        knn_updated.fit(X_scaled_updated)

        # Save retrained model
        joblib.dump(knn_updated, knn_model_cpu_file_path)
        return jsonify({'message': f'Model trained successfully with k: {k_neighbors}'})
    except Exception:
        return None
            



def retrain_knn_model_by_params_weight(k):
    try:

        # Load data from the database for retraining
        with pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASS, db=DB_NAME, charset='utf8',
                             cursorclass=pymysql.cursors.DictCursor) as mysql_connection:
            query_all_products = "SELECT p.id, p.name," \
                "MAX(CASE WHEN a.name = 'Weight' THEN pa.value END) AS weight " \
                "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                "INNER JOIN products p ON p.id = pa.product_id " \
                "INNER JOIN product_price pp ON pp.product_id = p.id " \
                "GROUP BY p.name"
            updated_df = fetch_all_product_data(
                mysql_connection, query_all_products)

        # Encode categorical data
        categorical_cols = ['weight']
        updated_df[categorical_cols] = updated_df[categorical_cols].astype(
            'category')
        updated_df[categorical_cols] = updated_df[categorical_cols].apply(
            lambda x: x.cat.codes)

        # Prepare input data for KNN
        X_updated = updated_df[['weight']]
        scaler_updated = StandardScaler()
        X_scaled_updated = scaler_updated.fit_transform(X_updated)
        k_neighbors = int(k)

        # Retrain KNN model
        knn_updated = NearestNeighbors(
            n_neighbors=k_neighbors, algorithm='auto')
        knn_updated.fit(X_scaled_updated)

        # Save retrained model
        joblib.dump(knn_updated, knn_model_weight_file_path)
        return jsonify({'message': f'Model trained successfully with k: {k_neighbors}'})

    except Exception as e:
        return {'error': str(e)}, 500


def retrain_knn_model_by_params_screen(k):
    try:
        # Load data from the database for retraining
        with pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASS, db=DB_NAME, charset='utf8',
                             cursorclass=pymysql.cursors.DictCursor) as mysql_connection:
            query_all_products = "SELECT p.id, p.name," \
                "MAX(CASE WHEN a.name = 'Screen' THEN pa.value END) AS screen " \
                "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                "INNER JOIN products p ON p.id = pa.product_id " \
                "INNER JOIN product_price pp ON pp.product_id = p.id " \
                "GROUP BY p.name"
            updated_df = fetch_all_product_data(
                mysql_connection, query_all_products)

        # Encode categorical data
        categorical_cols = ['screen']
        updated_df[categorical_cols] = updated_df[categorical_cols].astype(
            'category')
        updated_df[categorical_cols] = updated_df[categorical_cols].apply(
            lambda x: x.cat.codes)

        # Prepare input data for KNN
        X_updated = updated_df[['screen']]
        scaler_updated = StandardScaler()
        X_scaled_updated = scaler_updated.fit_transform(X_updated)
        k_neighbors = int(k)

        # Retrain KNN model
        knn_updated = NearestNeighbors(
            n_neighbors=k_neighbors, algorithm='auto')
        knn_updated.fit(X_scaled_updated)

        # Save retrained model
        joblib.dump(knn_updated, knn_model_screen_file_path)
        return jsonify({'message': f'Model trained successfully with k: {k_neighbors}'})

    except Exception as e:
        return {'error': str(e)}, 500


def retrain_knn_model_by_params_ram(k):
    try:
        # Load data from the database for retraining
        with pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASS, db=DB_NAME, charset='utf8',
                             cursorclass=pymysql.cursors.DictCursor) as mysql_connection:
            query_all_products = "SELECT p.id, p.name," \
                "MAX(CASE WHEN a.name = 'RAM' THEN pa.value END) AS ram " \
                "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                "INNER JOIN products p ON p.id = pa.product_id " \
                "INNER JOIN product_price pp ON pp.product_id = p.id " \
                "GROUP BY p.name"
            updated_df = fetch_all_product_data(
                mysql_connection, query_all_products)

        # Encode categorical data
        categorical_cols = ['ram']
        updated_df[categorical_cols] = updated_df[categorical_cols].astype(
            'category')
        updated_df[categorical_cols] = updated_df[categorical_cols].apply(
            lambda x: x.cat.codes)

        # Prepare input data for KNN
        X_updated = updated_df[['ram']]
        scaler_updated = StandardScaler()
        X_scaled_updated = scaler_updated.fit_transform(X_updated)
        k_neighbors = int(k)

        # Retrain KNN model
        knn_updated = NearestNeighbors(
            n_neighbors=k_neighbors, algorithm='auto')
        knn_updated.fit(X_scaled_updated)

        # Save retrained model
        joblib.dump(knn_updated, knn_model_ram_file_path)
        return jsonify({'message': f'Model trained successfully with k: {k_neighbors}'})

    except Exception as e:
        return {'error': str(e)}, 500


def retrain_knn_model_by_params_storage(k):
    try:
        # Load data from the database for retraining
        with pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASS, db=DB_NAME, charset='utf8',
                             cursorclass=pymysql.cursors.DictCursor) as mysql_connection:
            query_all_products = "SELECT p.id, p.name," \
                "MAX(CASE WHEN a.name = 'Storage' THEN pa.value END) AS storage " \
                "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                "INNER JOIN products p ON p.id = pa.product_id " \
                "INNER JOIN product_price pp ON pp.product_id = p.id " \
                "GROUP BY p.name"
            updated_df = fetch_all_product_data(
                mysql_connection, query_all_products)

        # Encode categorical data
        categorical_cols = ['storage']
        updated_df[categorical_cols] = updated_df[categorical_cols].astype(
            'category')
        updated_df[categorical_cols] = updated_df[categorical_cols].apply(
            lambda x: x.cat.codes)

        # Prepare input data for KNN
        X_updated = updated_df[['storage']]
        scaler_updated = StandardScaler()
        X_scaled_updated = scaler_updated.fit_transform(X_updated)
        k_neighbors = int(k)

        # Retrain KNN model
        knn_updated = NearestNeighbors(
            n_neighbors=k_neighbors, algorithm='auto')
        knn_updated.fit(X_scaled_updated)

        # Save retrained model
        joblib.dump(knn_updated, knn_model_storage_file_path)
        return jsonify({'message': f'Model trained successfully with k: {k_neighbors}'})

    except Exception as e:
        return {'error': str(e)}, 500


def retrain_knn_model_by_params_graphics(k):
    try:
        # Load data from the database for retraining
        with pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASS, db=DB_NAME, charset='utf8',
                             cursorclass=pymysql.cursors.DictCursor) as mysql_connection:
            query_all_products = "SELECT p.id, p.name," \
                "MAX(CASE WHEN a.name = 'Graphics' THEN pa.value END) AS graphics " \
                "FROM product_attributes pa " \
                "INNER JOIN attributes a ON a.id = pa.attribute_id " \
                "INNER JOIN products p ON p.id = pa.product_id " \
                "INNER JOIN product_price pp ON pp.product_id = p.id " \
                "GROUP BY p.name"
            updated_df = fetch_all_product_data(
                mysql_connection, query_all_products)

        # Encode categorical data
        categorical_cols = ['graphics']
        updated_df[categorical_cols] = updated_df[categorical_cols].astype(
            'category')
        updated_df[categorical_cols] = updated_df[categorical_cols].apply(
            lambda x: x.cat.codes)

        # Prepare input data for KNN
        X_updated = updated_df[['graphics']]
        scaler_updated = StandardScaler()
        X_scaled_updated = scaler_updated.fit_transform(X_updated)
        k_neighbors = int(k)

        # Retrain KNN model
        knn_updated = NearestNeighbors(
            n_neighbors=k_neighbors, algorithm='auto')
        knn_updated.fit(X_scaled_updated)

        # Save retrained model
        joblib.dump(knn_updated, knn_model_graphics_file_path)
        return jsonify({'message': f'Model trained successfully with k: {k_neighbors}'})

    except Exception as e:
        return {'error': str(e)}, 500

def retrain_knn_model_by_params_price(k):
    try:
        # Load data from the database for retraining
        with pymysql.connect(host=DB_HOST, user=DB_USER, password=DB_PASS, db=DB_NAME, charset='utf8',
                             cursorclass=pymysql.cursors.DictCursor) as mysql_connection:
            query_all_products = "SELECT p.id, p.name,p.price" \
                "FROM product_attributes pa INNER JOIN `attributes` a ON a.id = pa.attribute_id " \
                "INNER JOIN products p ON p.id = pa.product_id " \
                "INNER JOIN product_price pp ON pp.product_id = p.id " \
                "GROUP BY p.name"
            updated_df = fetch_all_product_data(
                mysql_connection, query_all_products)

        # Encode categorical data
        categorical_cols = ['price']
        updated_df[categorical_cols] = updated_df[categorical_cols].astype(
            'category')
        updated_df[categorical_cols] = updated_df[categorical_cols].apply(
            lambda x: x.cat.codes)

        # Prepare input data for KNN
        X_updated = updated_df[['price']]
        scaler_updated = StandardScaler()
        X_scaled_updated = scaler_updated.fit_transform(X_updated)
        k_neighbors = int(k)

        # Retrain KNN model
        knn_updated = NearestNeighbors(
            n_neighbors=k_neighbors, algorithm='auto')
        knn_updated.fit(X_scaled_updated)

        # Save retrained model
        joblib.dump(knn_updated, knn_model_price_file_path)
        return jsonify({'message': f'Model trained successfully with k: {k_neighbors}'})

    except Exception as e:
        return {'error': str(e)}, 500
# region private Method
def fetch_product_data(connection, query, productId):
    with connection.cursor() as cursor:
        cursor.execute(query, productId)
        rows = cursor.fetchall()

    return pd.DataFrame(rows)


def fetch_all_product_data(connection, query):
    with connection.cursor() as cursor:
        cursor.execute(query)
        rows = cursor.fetchall()

    return pd.DataFrame(rows)


def preprocess_data(product_df):
    numeric_fields = ['rating']
    product_df[numeric_fields] = product_df[numeric_fields].astype(int)

    categorical_cols = ['cpu', 'graphics',
                        'ram', 'screen', 'storage', 'weight']
    product_df[categorical_cols] = product_df[categorical_cols].astype(
        'category').apply(lambda x: x.cat.codes)

    return product_df


def preprocess_data_by_params(product_df, param):
    data = f"{param}"
    categorical_cols = [data]
    product_df[categorical_cols] = product_df[categorical_cols].astype(
        'category').apply(lambda x: x.cat.codes)

    return product_df


def find_similar_products(product_df, knn):
    new_product_info = product_df.drop(columns=['id', 'name'])

    scaler = StandardScaler()
    new_product_info_scaled = scaler.fit_transform(new_product_info)

    distances, indices = knn.kneighbors(new_product_info_scaled)

    similar_products_indices = indices[0]
    similar_products = product_df.iloc[similar_products_indices]

    return similar_products

# endregion
