
from elasticsearch import Elasticsearch
import pandas as pd
import datetime
import json
from flask import jsonify
from tqdm import tqdm
import base64
import socket
import requests


def push_data_to_elasticsearch(data, index_name):
    print(f"Processing {len(data)} bulk data to elasticsearch...")
    # Elasticsearch authentication credentials
    username = "elastic"
    password = "NTH@13042000"

    # Encode the username and password for basic authentication
    credentials = base64.b64encode(
        f"{username}:{password}".encode("utf-8")).decode("utf-8")
    headers = {"Authorization": f"Basic {credentials}"}

    # Connect to Elasticsearch with authentication
    es = Elasticsearch(
        [{'host': '192.168.1.10', 'port': 9200, 'scheme': 'http'}],
        headers=headers
    )

    try:
        for doc in data:
            es.index(index=index_name, body=doc)
        print(f"Total documents inserted into '{index_name}': {len(data)}")
    except Exception as e:
        print(f"Error bulk indexing remaining documents: {e}")


def find_lat_long_by_ip(ip, index_name):
    print(f"Processing find data to elasticsearch...")
    # Elasticsearch authentication credentials
    username = "elastic"
    password = "NTH@13042000"

    # Encode the username and password for basic authentication
    credentials = base64.b64encode(
        f"{username}:{password}".encode("utf-8")).decode("utf-8")
    headers = {"Authorization": f"Basic {credentials}"}

    # Connect to Elasticsearch with authentication
    es = Elasticsearch(
        [{'host': '192.168.1.10', 'port': 9200, 'scheme': 'http'}],
        headers=headers
    )
    try:

        # define the search query
        query = {
            "size": 1,
            "_source": ["location.coordinates"],
            "query": {
                "match_phrase": {
                    "source_ip": "172.70.135.208"
                }
            }
        }

        # search for documents
        response = es.search(index=index_name, body=query)
        if (response['hits']['total']['value'] > 0):
            return (response['hits']['hits'][0]['_source']['location']['coordinates'][1],response['hits']['hits'][0]['_source']['location']['coordinates'][0])
        else:
            return 0
    except Exception as e:
        print(f"Error bulk indexing remaining documents: {e}")


data = [{"Source IP": "172.70.135.208", "Destination IP": "192.168.1.10", "Source Port": 39872, "Destination Port": 443, "Protocol": 6, "TimeSpan": "2024-04-17 07:56:44", "Flow Duration": 34.0938568115, "Total Fwd Packets": 1, "Total Backward Packets": 1, "Total Length of Fwd Packets": 66, "Total Length of Bwd Packets": 66, "Fwd Packet Length Max": 66, "Fwd Packet Length Min": 66, "Fwd Packet Length Mean": 66.0, "Fwd Packet Length Std": 0.0, "Bwd Packet Length Max": 66, "Bwd Packet Length Min": 66, "Bwd Packet Length Mean": 66.0, "Bwd Packet Length Std": 0.0, "Flow Bytes\/s": 3871665.230769231, "Flow Packets\/s": 58661.5944055944, "Flow IAT Mean": 0, "Flow IAT Std": 0, "Flow IAT Max": 0, "Flow IAT Min": 0, "Fwd IAT Total": 0, "Fwd IAT Mean": 0, "Fwd IAT Std": 0, "Fwd IAT Max": 0, "Fwd IAT Min": 0, "Bwd IAT Total": 0, "Bwd IAT Mean": 0, "Bwd IAT Std": 0, "Bwd IAT Max": 0, "Bwd IAT Min": 0, "Fwd PSH Flags": 0, "Bwd PSH Flags": 0, "Fwd URG Flags": 0, "Bwd URG Flags": 0, "Fwd Header Length": 20, "Bwd Header Length": 20,
         "Fwd Packets\/s": 29330.7972027972, "Bwd Packets\/s": 29330.7972027972,
         "Min Packet Length": 66, "Max Packet Length": 66, "Packet Length Mean": 66.0, "Packet Length Std": 0.0, "Packet Length Variance": 0.0, "FIN Flag Count": 0, "SYN Flag Count": 0, "RST Flag Count": 0, "PSH Flag Count": 0, "ACK Flag Count": 2, "URG Flag Count": 0, "CWE Flag Count": 0, "ECE Flag Count": 0, "Down\/Up Ratio": 1.0, "Average Packet Size": 66.0, "Avg Fwd Segment Size": 66.0, "Avg Bwd Segment Size": 66.0, "Fwd Header Length.1": 20, "Fwd Avg Bytes\/Bulk": 0, "Fwd Avg Packets\/Bulk": 0, "Fwd Avg Bulk Rate": 0, "Bwd Avg Bytes\/Bulk": 0, "Bwd Avg Packets\/Bulk": 0,
         "Bwd Avg Bulk Rate": 0, "Subflow Fwd Packets": 1, "Subflow Fwd Bytes": 66, "Subflow Bwd Packets": 1, "Subflow Bwd Bytes": 66, "Init_Win_bytes_forward": 8, "Init_Win_bytes_backward": 501, "act_data_pkt_fwd": 0, "min_seg_size_forward": 20, "Active Mean": 0, "Active Std": 0, "Active Max": 0, "Active Min": 0, "Idle Mean": 0,
         "Idle Std": 0, "Idle Max": 0, "Idle Min": 0, "location": {
             "type": "Point",
             "coordinates": [-74.4918, 37.1895,]

         }}]
# Convert keys to lowercase, trim whitespace, and replace spaces with underscores
parsed_data = [{k.lower().strip().replace(' ', '_')
                .replace('fwd_packets\\/s', 'fwd_avg_bytes_bulk')
                .replace('flow_bytes\\/s', 'flow_bytes_s')
                .replace('flow_packets\\/s', 'flow_packets_s')
                .replace('bwd_packets\\/s', 'bwd_packets_s')
                .replace('down\\/up_ratio', 'down_up_ratio')
                .replace('fwd_avg_bytes\\/bulk', 'fwd_avg_bytes_bulk')
                .replace('fwd_avg_packets\\/bulk', 'fwd_avg_packets_bulk')
                .replace('bwd_avg_bytes\\/bulk', 'bwd_avg_bytes_bulk')
                .replace('fwd_header_length.1', 'fwd_header_length_1')

                .replace('bwd_avg_packets\\/bulk', 'bwd_avg_packets_bulk'): v for k, v in d.items()} for d in data]

# Add 'timespan' field with current datetime
for obj in parsed_data:
    obj['creation_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

# print(parsed_data)
# print(parsed_data)
# push_data_to_elasticsearch(parsed_data, "anomaly")

result = find_lat_long_by_ip(parsed_data[0]['source_ip'], "anomaly")

ip_add = '172.70.135.208'  # 198.35.26.96
rs = printDetails(ip_add)
print(result)

print(rs)
